from .distributable_item import DistributableItem, WeightError

def print_working():
    print("post_service is working!")

def nurdy_number():
    return 42

def ask_user_for_positive_number():
    while True:
        try:
            getal = int(input("getal\n"))
            return getal
            if getal > 0:
                return getal
            else:
                print("Positief graag")
        except ValueError:
            print("Dat is geen integer")

def weight_test():
    packet = DistributableItem(50, "Jeroen")
    weight = ask_user_for_positive_number()
    try:
        packet.weight = weight
        print("We gaan verder")
    except WeightError as e:
        print("Gewicht wordt niet geaccepteerd")
        raise e


def main():
    try:
        weight_test()
    except Exception as e:
        print(e)
