from .distributable_item import DistributableItem

class PostPackage(DistributableItem):
    def __init__(self, weight, sender, ribbon_color):
        super().__init__(weight=weight,
                         sender=sender)
        self._ribbon_color = ribbon_color

    def print(self):
        super().print()
        print("Post package")

