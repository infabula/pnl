"""Module docstring here"""
class WeightError(Exception):
    pass


class DistributableItem:
    """Class docstring here"""

    def __init__(self, weight, sender):
        self.weight = weight
        self._sender = sender

        
    # ----------- WEIGHT property -----
    def get_weight(self):
        return self._weight

    
    def set_weight(self, value):
        if value < 0:
            raise WeightError("Weight should be positive")
        else:
            self._weight = value

    weight = property(get_weight, set_weight)

    #-----------------------------------
    def print(self):
        print("Dist item")
